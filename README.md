# Climate Change Explained to Frogs Comic

![Cover](layout/p1.png)

Sources files for [Climate Change Explained to Frogs Comic](http://nylnook.com/en/comics/), and to allow translations.  
Created with [Krita](https://krita.org/) and [Inkscape](https://inkscape.org)   
Export script is using Bash 4, Exiftools, Inkscape, ImageMagick, Gostscript, Sed, Calibre, Zip and and Unzip

A [Making Of is available for a better understanding of this source repository](http://nylnook.com/en/blog/making-a-comic-from-A-to-Z-with-free-software/).

**Krita files (.kra) can't be hosted here (would exceed disk quota), I'm looking for another solution.**

Fonts
=====

You will need theses fonts installed on your computer in order to view and edit the .svg files correctly  
[Comili](http://nylnook.com/en/blog/comili-a-libre-comic-font/) - SIL Open Font License  
[Lets trace](https://fontlibrary.org/en/font/lets-trace) - SIL Open Font License  

License
=======

© Camille Bissuel a.k.a. Nylnook [nylnook.com](http://nylnook.com)  
Artwork and text is licensed under a [Creative Commons Attribution-ShareAlike 4.0 International License](https://creativecommons.org/licenses/by-sa/4.0/)  
Code (bash script) is licensed under the [MIT License](https://opensource.org/licenses/MIT)  

## Translators and correctors:

* French: Camille Bissuel, Sophie Bissuel
* English: Camille Bissuel, Allen Martsch
* Breizh : Patrick Béchard
