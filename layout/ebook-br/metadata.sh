#!/bin/sh

# Metadata
creator='Camille Bissuel (nylnook)'
url="http://nylnook.com"
year=`date +'%Y'`
copyright="Copyright (c) $creator $year"
licence="Dindan aotre emañ an oberenn-mañ Creative Commons Attribution-ShareAlike 4.0 International License."
pubDate=`date +%Y-%m`
language=br
title="Ar cheñchamant hin displeget d'ar gleskered"
tags="hin, gleskered, bannoù treset, cop21, cheñchamant hin, creative commons"
description="Komz a ra an holl diwar e benn, met n'ho peus ket komprenet gwall vat e pelec'h emañ an dalc'h dre just. Daoust petra eo ar sapre cheñchamant hin-se ?

Pe, er c'hontrol, komprenet pep tra ho peus, met en dizesper oc'h da lakaat ho mignoned da gompren petra eo, kar n'int ket evit intent seurt en dra-se ... ?

Gwiskit botoù ur glesker, splujit e-barzh ar gasterolenn, Sklaeroc'h e vo pep tra... ha tommoc'h !

Gant an 8 pajennad bannoù treset-mañ e vez diverret en ur mod peuz-skiantel ha diwir, hep mont re bell ganti 'vat, ar pezh a rank an nen kompren diwar-benn an dalc'h-se ... Hag eñ pouezus un disterañ evit hom amzer da zont !

N'omp ket klimatologourien holl. Hag evel e vez stadet er Guardian , « An darn vrasañ deus an dud a intent ar bed gant istorioù ha skeudennoù, ha n'eo ket gant listennadoù niveroù, dielfennoù statistikel, peotramant grafoù teknikel. Kement-se zo kaoz eo pouezus-tre kaout doareoù da droiñ ha da zisplegañ yezh chimik an dezrevelloù skiantel en ur mod dedennusoc'h »."
ebookIsbn="979-10-95663-07-2"

# Generated directories
dirwebjpg=web-jpg
dirHDjpg=hd-jpg
dirverticalstrip=vertical
direbooks=ebooks
dirHDebooks=ebooks-hd
dirprintrgb=print-rgb
dirprintcmyk=print-cmyk
