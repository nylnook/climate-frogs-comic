#!/bin/sh

# Metadata
creator='Camille Bissuel (nylnook)'
url="http://nylnook.com"
year=`date +'%Y'`
copyright="Copyright (c) $creator $year"
licence="This book is licensed under a Creative Commons Attribution-ShareAlike 4.0 International License."
pubDate=`date +%Y-%m`
language=en
title="Climate Change Explained to Frogs"
tags="comics, graphic novel, climate, climate change, creative commons, libre, free"
description="Everyone talks about it, but you don't really get the problem exactly, what is this famous climate change?

Or, on the contrary, you understood everything, but you despair to make it clear to your friends, because they don't give a damn ?

Put yourself in the skin of a frog, dive into the pot, everything will become clearer ... and hotter!

This short 8 pages comic summarizes, in a scientifically inaccurate and approximate way, exaggerating just a little bit, what you need to understand about this just-a-little-critical-for-our-future issue!

Everyone is not a climatologist. As noted by the Guardian, \"Most people understand the world through stories and images, not lists of numbers, probability statements or technical graphs, and so it is crucial to find ways of translating and interpreting the technical language found in scientific reports into something more engaging.\""
ebookIsbn="979-10-95663-01-0"

# Generated directories
dirwebjpg=web-jpg
dirHDjpg=hd-jpg
dirverticalstrip=vertical
direbooks=ebooks
dirprintrgb=print-rgb
dirprintcmyk=print-cmyk
