#!/bin/sh

# Metadata
creator='Camille Bissuel (nylnook)'
url="http://nylnook.com"
year=`date +'%Y'`
copyright="Copyright (c) $creator $year"
licence="This image is licensed under a Creative Commons Attribution-ShareAlike 4.0 International License."
pubDate=`date +%Y-%m`

title="Le changement climatique expliqué aux grenouilles"
ebookIsbn="979-10-95663-02-7"

# Generated directories
dirwebjpg=web-jpg
dirverticalstrip=vertical
direbooks=ebooks
dirprintrgb=print-rgb
dirprintcmyk=print-cmyk
