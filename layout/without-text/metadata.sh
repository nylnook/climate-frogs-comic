#!/bin/sh

# Metadata
creator='Camille Bissuel (nylnook)'
url="http://nylnook.com"
year=`date +'%Y'`
copyright="Copyright (c) $creator $year"
licence="This image is licensed under a Creative Commons Attribution-ShareAlike 4.0 International License."
pubDate=`date +%Y-%m`
language=en
title="Climate Change Explained to Frogs - version without text for translation"
ebookIsbn="979-10-95663-01-0"

# Generated directories
dirwebjpg=web-jpg
dirHDjpg=hd-jpg
dirverticalstrip=vertical
direbooks=ebooks
dirprintrgb=print-rgb
dirprintcmyk=print-cmyk
