# Le changement climatique
# expliqué aux grenouilles

Dans une grande casserole, il y avait 3 grenouilles :
- 1 grenouille occidentale, qui s'appelait Burt
- 1 grenouille « émergente », qui s'appelait Fu
- 1 grenouille du sud, qui s'appelait Dodji

Après la nuit, les grenouilles étaient enfin libres et égales en droit, et les mouches étaient abondantes, bien qu’inégalement réparties.
Ce matin, il faisait plutôt bon vivre dans la casserole.
Mais Burt trouva le bouton du gaz, et il se dit qu’il avait bien droit au confort moderne. Il y prit goût, sans prendre garde qu’une petite pellicule de gaz et de vapeur d’eau commençait à former un couvercle sur la casserole.
Ça faisait envie à Fu, et il se mit à vendre à Burt tout un tas de gadgets en échange de confort moderne, parce que lui aussi il y avait droit.
Fu engagea Dodji pour faire les incessants allers-retours de gadgets entre lui et Burt, contre le très équitable droit à un meilleur accès aux mouches.
Mais les usines à gadgets avaient besoin de gaz, et les déplacements de Dodji créaient encore plus de vapeur d'eau.
Le gaz était donc de plus en plus fort, et le couvercle de plus en plus épais. Il faisait très progressivement de plus en plus chaud. Burt s'est bien dit que ça pourrait peut-être être un problème, mais il aimait l'eau chaude.
Puis les mouches commencèrent à tomber, et des remous agitait régulièrement la casserole, alors les trois grenouilles se réunirent, admirent qu’il y avait un problème, et décidèrent qu’elles ne changeraient rien. De toute façon, les mouches de synthèse de Fu avaient meilleur goût.
Le couvercle bien épais se solidifia donc dans l’idée de rester quelques heures sur place, de continuer à s’épaissir, et d’être encore plus étanche.
Burt se mit à chercher d’autres boutons de gaz parce que son obésité l’obligeait à plus de confort.
Fu quant à lui avait maintenant du confort, mais tout autour de lui avait une désagréable odeur de gaz. 
Dodji, lui, trouvait que le fond de la casserole devenait invivable, et migra illégalement vers la surface.
Il y eu d’autres remous, et les trois grenouilles commencèrent à avoir des rougeurs, alors elles se réunirent encore une fois pour savoir si oui ou non elles pouvaient se permettre que l’eau de la casserole entre en ébullition. Il faisait déjà 45 °C.
Burt dit que 20°C de plus était la limite à ne pas dépasser, mais qu’il ne renoncerait pas à son confort.
Fu dit que 30°C de plus et une odeur de gaz était tout à supportable, qu’il comptait bien profiter du confort qu’il avait chèrement gagné, et qu’il pouvait fournir des mouches synthétiques à tous les habitants de la casserole.
Dodgi dit qu’il fallait juste couper le gaz et lui laisser sa part de mouches sauvages, mais les deux autre firent semblant de ne pas avoir entendu.
Au restaurant de la galaxie, on annonce que de la grenouille bouillie, cuite à la casserole-Terre, sera au menu ce midi. Heureusement, personne n’est à l’abri d’une coupure de gaz.



# Strip Résumé

Dans une grande casserole, il y avait 3 grenouilles
Une d'entre elle trouva le bouton du gaz, et se dit qu’elle avait bien droit au confort moderne
Il y eu des remous, et les trois grenouilles commencèrent à avoir des rougeurs, alors elles se réunirent pour savoir si oui ou non elles pouvaient se permettre que l’eau de la casserole entre en ébullition...
